#pragma once

#include <await/coroutine/routine.hpp>

#include <context/context.hpp>

#include <exception>

namespace await::coroutine {

// Stackful asymmetric coroutine

class Coroutine {
 public:
  Coroutine(Routine routine, wheels::MemSpan stack);

  void Resume();

  // Suspend current coroutine
  static void Suspend();

  bool IsCompleted() const {
    return completed_;
  }

 private:
  [[noreturn]] static void Trampoline();

  // In trampoline:
  void Start();
  void Complete();

  void DoSuspend();

 private:
  Routine routine_;
  context::ExecutionContext context_;
  context::ExecutionContext caller_context_;
  bool completed_{false};
  std::exception_ptr exception_ptr_;
};

}  // namespace await::coroutine
