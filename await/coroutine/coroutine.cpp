#include <await/coroutine/coroutine.hpp>

#include <wheels/support/assert.hpp>
#include <wheels/support/compiler.hpp>
#include <wheels/support/exchange.hpp>

namespace await::coroutine {

static thread_local Coroutine* current = nullptr;

Coroutine::Coroutine(Routine routine, wheels::MemSpan stack)
    : routine_(std::move(routine)) {
  context_.Setup(stack, Trampoline);
}

void Coroutine::Trampoline() {
  Coroutine* coroutine = current;

  coroutine->Start();

  try {
    coroutine->routine_();
  } catch (...) {
    coroutine->exception_ptr_ = std::current_exception();
  }

  coroutine->Complete();  // Never returns
  WHEELS_UNREACHABLE();
}

void Coroutine::Resume() {
  WHEELS_VERIFY(!completed_, "Coroutine completed");

  {
    auto guard = wheels::ScopedExchange(current, this);
    caller_context_.SwitchTo(context_);
  }

  if (exception_ptr_ != nullptr) {
    std::rethrow_exception(exception_ptr_);
  }
}

void Coroutine::Suspend() {
  WHEELS_VERIFY(current, "Not in coroutine");
  current->DoSuspend();
}

void Coroutine::DoSuspend() {
  context_.SwitchTo(caller_context_);
}

void Coroutine::Start() {
  context_.AfterStart();
}

void Coroutine::Complete() {
  completed_ = true;
  context_.ExitTo(caller_context_);
}

}  // namespace await::coroutine
