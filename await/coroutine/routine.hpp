#pragma once

#include <wheels/support/function.hpp>

namespace await::coroutine {

using Routine = wheels::UniqueFunction<void()>;

}  // namespace await::coroutine
