#pragma once

#include <utility>

namespace await::support {

// TODO: Limit to spinlock?

template <typename Mutex>
class MutexHolder {
 public:
  explicit MutexHolder(Mutex& mutex) : mutex_(&mutex) {
    mutex_->lock();
  }

  // Movable

  MutexHolder(MutexHolder&& that) : mutex_(that.Release()) {
  }

  MutexHolder& operator=(MutexHolder&& that) {
    Unlock();
    mutex_ = that.Release();
    return *this;
  }

  // Non-copyable

  MutexHolder(const MutexHolder& that) = delete;
  MutexHolder& operator=(const MutexHolder& that) = delete;

  // Manual unlock

  void Unlock() {
    // NB: do not write to MutexHolder memory after unlock!
    if (Mutex* mutex = Release(); mutex != nullptr) {
      mutex->unlock();
    }
  }

  void unlock() {
    Unlock();
  }

  ~MutexHolder() {
    Unlock();
  }

 private:
  Mutex* Release() {
    return std::exchange(mutex_, nullptr);
  }

 private:
  Mutex* mutex_;
};

}  // namespace await::support
