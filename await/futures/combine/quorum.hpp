#pragma once

#include <await/futures/promise.hpp>
#include <await/futures/combine/detail/combine.hpp>
#include <await/futures/combine/detail/traits.hpp>
#include <await/futures/helpers.hpp>

#include <await/support/thread_spinlock.hpp>

#include <twist/stdlike/atomic.hpp>

#include <vector>

namespace await::futures {

// Quorum

//////////////////////////////////////////////////////////////////////

namespace detail {

// TODO: threshold = 0

template <typename T>
class QuorumCombinator {
  using Traits = CombinedTypeTraits<T>;

  using ValuesVector = typename Traits::ValuesVector;
  using ValuesVectorBuilder = typename Traits::ValuesVectorBuilder;

 public:
  QuorumCombinator(size_t num_inputs, size_t threshold)
      : num_inputs_(num_inputs), threshold_(threshold) {
    values_.Reserve(threshold_);  // Be optimistic
  }

  void ProcessInput(Result<T> result, size_t /*index*/) {
    std::unique_lock lock(mutex_);

    if (completed_) {
      return;
    }

    if (result.IsOk()) {
      ++oks_;
      values_.Add(std::move(result));
      if (oks_ == threshold_) {
        // Quorum reached
        completed_ = true;
        auto values_result = std::move(values_).MakeResult();
        lock.unlock();
        std::move(promise_).Set(std::move(values_result));
      }
    } else {
      ++errors_;
      if (ImpossibleToReachThreshold(errors_)) {
        completed_ = true;
        lock.unlock();
        std::move(promise_).SetError(result.GetError());
      }
    }
  }

  Future<ValuesVector> MakeFuture() {
    return std::move(promise_.MakeFuture());
  }

 private:
  bool ImpossibleToReachThreshold(size_t errors) const {
    return errors + threshold_ > num_inputs_;
  }

 private:
  const size_t num_inputs_;
  const size_t threshold_;

  support::ThreadSpinLock mutex_;
  size_t oks_{0};
  size_t errors_{0};
  ValuesVectorBuilder values_;
  Promise<ValuesVector> promise_;
  bool completed_{false};
};

}  // namespace detail

// First 'threshold' values or last error that blocks quorum
// std::vector<Future<T>> -> Future<std::vector<T>> (or -> Future<void> if T ==
// void)

template <typename T>
auto Quorum(std::vector<Future<T>> inputs, size_t threshold) {
  // Corner cases
  if (threshold == 0) {
    return detail::CombinedTypeTraits<T>::MakeEmptyOutput();
  }
  if (inputs.size() < threshold) {
    WHEELS_PANIC(
        "Number of inputs < required threshold, output future never completes");
  }

  return detail::Combine<detail::QuorumCombinator>(std::move(inputs),
                                                   threshold);
}

//////////////////////////////////////////////////////////////////////

}  // namespace await::futures
