#include <await/futures/helpers.hpp>

namespace await::futures {

Future<void> MakeCompletedVoid() {
  auto [f, p] = MakeContract<void>();
  std::move(p).Set();
  return std::move(f);
}

detail::Failure MakeError(Error&& error) {
  return detail::Failure{std::move(error)};
}

}  // namespace await::futures
