#pragma once

#include <context/stack.hpp>

namespace await::fibers {

using Stack = context::Stack;

Stack AllocateStack();
void ReleaseStack(Stack&& stack);

}  // namespace await::fibers
