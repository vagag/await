#pragma once

#include <optional>
#include <string>
#include <any>
#include <map>

namespace await::fibers {

// Poor man's FLS

using FLS = std::map<std::string, std::any>;

namespace self {

void SetLocal(const std::string& key, std::any value);

std::any GetLocalImpl(const std::string& key);

template <typename T>
std::optional<T> GetLocal(const std::string& key) {
  auto value = GetLocalImpl(key);
  if (value.has_value()) {
    return std::any_cast<T>(value);
  }
  return std::nullopt;
}

}  // namespace self

}  // namespace await::fibers
