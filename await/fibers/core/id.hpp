#pragma once

#include <wheels/support/id.hpp>

namespace await::fibers {

using FiberId = wheels::Id;

static const FiberId kInvalidFiberId = wheels::kInvalidId;

FiberId GenerateNewId();

// Workaround for deterministic simulation
void ResetIds();

}  // namespace await::fibers
