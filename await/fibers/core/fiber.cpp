#include <await/fibers/core/fiber.hpp>

#include <await/fibers/core/stack.hpp>

#include <wheels/support/assert.hpp>
#include <wheels/support/exception.hpp>
#include <wheels/support/exchange.hpp>
#include <wheels/support/id.hpp>

#include <await/support/thread_spinlock.hpp>

#include <atomic>

namespace await::fibers {

//////////////////////////////////////////////////////////////////////

static support::ThreadSpinLock alive_lock_;
static wheels::IntrusiveList<Fiber> alive_fibers_;

// core/guts.hpp
wheels::IntrusiveList<Fiber>& AliveFibers() {
  return alive_fibers_;
}

//////////////////////////////////////////////////////////////////////

static thread_local Fiber* current = nullptr;

Fiber* Fiber::AccessCurrent() {
  WHEELS_VERIFY(current != nullptr, "Not in fiber context");
  return current;
}

Fiber* Fiber::GetCurrent() {
  return current;
}

Fiber::Fiber(Routine routine, Stack stack, IExecutorPtr executor, FiberId id)
    : stack_(std::move(stack)),
      coroutine_(std::move(routine), stack_.AsMemSpan()),
      executor_(std::move(executor)),
      id_(id) {
  auto g = alive_lock_.Guard();
  alive_fibers_.PushFront(this);
}

Fiber::~Fiber() {
}

void Fiber::Spawn(Routine routine, IExecutorPtr e) {
  auto id = GenerateNewId();
  auto stack = AllocateStack();
  Fiber* fiber =
      new Fiber(std::move(routine), std::move(stack), std::move(e), id);
  fiber->Schedule();
}

void Fiber::Yield() {
  Stop();
}

void Fiber::TeleportTo(IExecutorPtr e) {
  executor_ = e;
  Stop();
}

void Fiber::Suspend(IAwaiter* awaiter) {
  awaiter_ = awaiter;
  Stop();
}

void Fiber::Resume() {
  Schedule();
}

void Fiber::Schedule() {
  executor_->Execute([this]() { Step(); });
}

void Fiber::Reschedule() {
  if (coroutine_.IsCompleted()) {
    // Completed
    Destroy();
  } else if (awaiter_) {
    // Suspended
    IAwaiter* awaiter = std::exchange(awaiter_, nullptr);
    // NB: `this` fiber may be resumed in another thread
    // before ScheduleResume returns control!
    awaiter->ScheduleResume(FiberHandle(this));
  } else {
    // Runnable
    Schedule();
  }
}

void Fiber::Destroy() {
  ReleaseStack(std::move(stack_));
  {
    auto g = alive_lock_.Guard();
    Unlink();
  }
  delete this;
}

void Fiber::Step() {
  try {
    auto guard = wheels::ScopedExchange(current, this);
    coroutine_.Resume();
  } catch (...) {
    WHEELS_PANIC("Unhandled exception in fiber (id = "
                 << id_ << "): " << wheels::CurrentExceptionMessage());
  }
  Reschedule();
}

void Fiber::Stop() {
  Coroutine::Suspend();
}

std::optional<std::string> Fiber::Name() const {
  return name_;
}

void Fiber::SetName(const std::string& name) {
  name_.emplace(name);
}

}  // namespace await::fibers
