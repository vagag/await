#pragma once

namespace await::fibers {

class Fiber;

// Lightweight trivially-copyable fiber handle

class FiberHandle {
 public:
  explicit FiberHandle(Fiber* f) : f_(f) {
  }

  FiberHandle() : FiberHandle(nullptr) {
  }

  static FiberHandle Invalid() {
    return FiberHandle(nullptr);
  }

  bool IsValid() const {
    return f_ != nullptr;
  }

  void Resume();

 private:
  Fiber* f_;
};

}  // namespace await::fibers
