#pragma once

#include <await/fibers/core/fiber.hpp>

namespace await::fibers {

// Do not use this functions!

// Not thread-safe!
wheels::IntrusiveList<Fiber>& AliveFibers();

}  // namespace await::fibers
