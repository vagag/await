#pragma once

#include <await/fibers/core/api.hpp>

#include <await/fibers/core/stack.hpp>
#include <await/coroutine/coroutine.hpp>
#include <await/executors/executor.hpp>
#include <await/fibers/core/awaiter.hpp>
#include <await/fibers/core/fls.hpp>

#include <twist/stdlike/atomic.hpp>

#include <wheels/support/intrusive_list.hpp>

#include <optional>
#include <string>

namespace await::fibers {

using coroutine::Coroutine;
using executors::IExecutorPtr;

using wheels::IntrusiveListNode;

//////////////////////////////////////////////////////////////////////

// Fiber = stackful coroutine + executor

class Fiber : public IntrusiveListNode<Fiber> {
  using Routine = coroutine::Routine;

 public:
  Fiber(Routine routine, Stack stack, IExecutorPtr executor, FiberId id);
  ~Fiber();

  static Fiber* AccessCurrent();
  static Fiber* GetCurrent();

  IExecutorPtr GetExecutor() {
    return executor_;
  }

  static void Spawn(Routine routine, IExecutorPtr e);

  void Yield();
  void TeleportTo(IExecutorPtr e);
  void Suspend(IAwaiter* awaiter);
  void Resume();

  FiberId Id() const {
    return id_;
  }

  std::optional<std::string> Name() const;
  void SetName(const std::string& name);

  FLS& GetFLS() {
    return fls_;
  }

  Stack& GetStack() {
    return stack_;
  }

  size_t GetSelectTwister() {
    return select_twister_.fetch_add(1);
  }

 private:
  void Stop();
  void Step();
  void Schedule();
  void Reschedule();
  void Destroy();

 private:
  Stack stack_;
  Coroutine coroutine_;
  IExecutorPtr executor_;
  FiberId id_;
  IAwaiter* awaiter_{nullptr};
  FLS fls_;
  std::optional<std::string> name_;
  // TODO: remove
  twist::stdlike::atomic<size_t> select_twister_{0};
};

}  // namespace await::fibers
