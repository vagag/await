#pragma once

#include <await/fibers/core/handle.hpp>

namespace await::fibers {

struct IAwaiter {
  virtual ~IAwaiter() = default;

  // ~ await_suspend in C++20 coroutines
  virtual void ScheduleResume(FiberHandle f) = 0;
};

}  // namespace await::fibers
