#pragma once

#include <await/executors/static_thread_pool.hpp>

#include <await/fibers/core/api.hpp>
#include <await/fibers/sync/thread_like.hpp>

#include <await/support/thread_event.hpp>

#include <cstdlib>

namespace await::fibers {

class TestRuntime {
 public:
  TestRuntime(size_t threads, const std::string& label = "test")
      : thread_pool_(executors::MakeStaticThreadPool(threads, label)) {
  }

  template <typename Routine>
  void Spawn(Routine&& routine) {
    fiber_count_.fetch_add(1);
    fibers::Spawn(Accounted(std::forward<Routine>(routine)), thread_pool_);
  }

  template <typename Routine>
  ThreadLike Thread(Routine&& routine) {
    fiber_count_.fetch_add(1);
    return ThreadLike(thread_pool_, Accounted(std::forward<Routine>(routine)));
  }

  void Join() {
    if (joined_) {
      return;
    }

    joining_.store(true);
    if (fiber_count_.load() > 0) {
      all_completed_.Await();
    }
    thread_pool_->Join();
    joined_ = true;
  }

  ~TestRuntime() {
    Join();
  }

 private:
  template <typename Routine>
  auto Accounted(Routine&& routine) {
    static_assert(std::is_convertible_v<Routine&&, FiberRoutine>,
                  "Fiber routine expected");

    return [this, routine = std::move(routine)]() mutable {
      routine();
      OnCompleted();
    };
  }

  void OnCompleted() {
    if (fiber_count_.fetch_sub(1) == 1 && joining_.load() == true) {
      all_completed_.Set();
    }
  }

 private:
  executors::IThreadPoolPtr thread_pool_;

  twist::stdlike::atomic<size_t> fiber_count_{0};
  twist::stdlike::atomic<bool> joining_{false};
  support::ThreadOneShotEvent all_completed_;
  bool joined_{false};
};

}  // namespace await::fibers
