#pragma once

#include <await/fibers/test/test_runtime.hpp>

#define AWAIT_EXAMPLE()                    \
  void AwaitExampleBody();                 \
  int main() {                             \
    await::fibers::TestRuntime runtime{4}; \
    runtime.Spawn([]() {                   \
      AwaitExampleBody();                  \
    });                                    \
    runtime.Join();                        \
    return 0;                              \
  }                                        \
  void AwaitExampleBody()
