#pragma once

#include <await/fibers/core/api.hpp>
#include <await/futures/future.hpp>

namespace await::fibers {

class ThreadLike {
 public:
  ThreadLike(IExecutorPtr e, FiberRoutine routine);

  // In fiber context
  ThreadLike(FiberRoutine routine);

  void Join();

  // TODO: Auto join in dtor?

 private:
  futures::Future<void> completed_;
};

}  // namespace await::fibers
