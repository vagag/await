#pragma once

#include <await/fibers/core/awaiter.hpp>
#include <await/fibers/core/handle.hpp>

#include <await/futures/future.hpp>

namespace await::fibers {

using futures::Future;
using wheels::Result;

namespace detail {

//////////////////////////////////////////////////////////////////////

template <typename T>
class FutureAwaiter : public IAwaiter {
 public:
  explicit FutureAwaiter(Future<T>&& future) : future_(std::move(future)) {
  }

  // ~ await_ready
  bool AwaitReady() {
    if (future_.IsReady()) {
      result_.emplace(std::move(future_).GetReadyResult());
      return true;
    }
    return false;
  }

  // ~ await_resume
  Result<T> GetResult() {
    return std::move(*result_);
  }

  // ~ await_suspend
  void ScheduleResume(FiberHandle f) override {
    std::move(future_).Subscribe([this, f](Result<T> result) mutable {
      result_.emplace(std::move(result));
      f.Resume();
    });
  }

 private:
  Future<T> future_;
  std::optional<Result<T>> result_;
};

}  // namespace detail

}  // namespace await::fibers

namespace await::futures {

template <typename T>
auto GetAwaiter(Future<T>&& f) {
  return fibers::detail::FutureAwaiter<T>(std::move(f));
}

}  // namespace await::futures
