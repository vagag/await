#pragma once

#include <await/fibers/sync/channel.hpp>
#include <await/fibers/core/check.hpp>

#include <variant>

// https://golang.org/src/runtime/chan.go
// https://golang.org/src/runtime/select.go

namespace await::fibers {

namespace detail {

size_t GetSelectTwister();

// Allocation-free!

template <typename X, typename Y>
class Selector {
  using Value = std::variant<X, Y>;

  template <typename T, size_t Index>
  struct Receiver : public ChannelImpl<T>::Receiver {
    Receiver(Selector* selector) : selector_(selector) {
    }

    bool Receive(T& value) override {
      return selector_->template TrySet<T, Index>(value);
    }

   private:
    Selector* selector_;
  };

  class Awaiter : public IAwaiter {
   public:
    Awaiter(Selector* selector) : selector_(selector) {
    }

    void ScheduleResume(FiberHandle f) {
      selector_->SetFiber(f);
    }

   private:
    Selector* selector_;
  };

 public:
  Value Select(Channel<X>& xs, Channel<Y>& ys) {
    auto xs_impl = xs.AccessImpl();
    auto ys_impl = ys.AccessImpl();

    Receiver<X, 0> x_receiver(this);
    Receiver<Y, 1> y_receiver(this);

    if (GetSelectTwister() % 2 == 0) {
      xs_impl->Receive(&x_receiver);
      ys_impl->Receive(&y_receiver);
    } else {
      ys_impl->Receive(&y_receiver);
      xs_impl->Receive(&x_receiver);
    }

    Awaiter awaiter(this);
    Suspend(&awaiter);

    // Resumed, value is ready!

    {
      auto guard = xs_impl->Lock();
      if (x_receiver.IsLinked()) {
        x_receiver.Unlink();
      }
    }

    {
      auto guard = ys_impl->Lock();
      if (y_receiver.IsLinked()) {
        y_receiver.Unlink();
      }
    }

    // TODO: ensure that channel does not hold pointer to x/y receiver at this
    // point

    return std::move(selected_value_);
  }

 private:
  template <typename T, size_t Index>
  bool TrySet(T& value) {
    std::unique_lock lock(mutex_);
    if (!set_) {
      selected_value_.template emplace<Index>(std::move(value));
      set_ = true;
      if (fiber_.IsValid()) {
        lock.unlock();
        fiber_.Resume();
      }
      return true;
    }
    return false;
  }

  void SetFiber(FiberHandle f) {
    std::unique_lock lock(mutex_);

    if (set_) {
      lock.unlock();
      f.Resume();
      return;
    }
    fiber_ = f;
  }

 private:
  support::ThreadSpinLock mutex_;
  bool set_{false};
  Value selected_value_;
  FiberHandle fiber_;
};

}  // namespace detail

/*
 * Usage:
 * Channel<Apple> apples;
 * Channel<Candy> candies;
 * ...
 * // value - std::variant<Apple, Candy>
 * auto value = Select(apples, candies);
 * switch (value.index()) {
 *   case 0:
 *     // Handle std::get<0>(value);
 *     break;
 *   case 1:
 *     // Handle std::get<1>(value);
 *     break;
 * }
 */

template <typename X, typename Y>
auto Select(Channel<X>& xs, Channel<Y>& ys) {
  I_EXPECT_AWAIT_FIBERS;

  return detail::Selector<X, Y>().Select(xs, ys);
}

}  // namespace await::fibers
