#pragma once

#include <await/fibers/sync/mutex.hpp>

namespace await::fibers {

// TODO: better impl

class CondVar {
  using Lock = std::unique_lock<Mutex>;

 public:
  void Wait(Lock& lock);

  void NotifyOne();
  void NotifyAll();

 private:
  FutexLike<size_t> count_{0};
};

}  // namespace await::fibers
