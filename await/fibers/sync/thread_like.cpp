#include <await/fibers/sync/thread_like.hpp>

// TODO: remove?
#include <await/fibers/core/fiber.hpp>

#include <await/futures/promise.hpp>
#include <await/futures/await.hpp>

namespace await::fibers {

ThreadLike::ThreadLike(IExecutorPtr e, FiberRoutine routine)
    : completed_(Future<void>::Invalid()) {
  auto [f, p] = futures::MakeContract<void>();

  completed_ = std::move(f);

  auto wrapper = [routine = std::move(routine), p = std::move(p)]() mutable {
    routine();
    std::move(p).Set();
  };

  Spawn(std::move(wrapper), e);
}

ThreadLike::ThreadLike(FiberRoutine routine)
    : ThreadLike(Fiber::AccessCurrent()->GetExecutor(), std::move(routine)) {
}

void ThreadLike::Join() {
  // Thread/fiber friendly
  futures::Await(std::move(completed_)).ExpectOk();
}

}  // namespace await::fibers
