#pragma once

#include <await/executors/executor.hpp>

// IExecutorPtr
#include <await/fibers/core/api.hpp>

namespace await::fibers {

//////////////////////////////////////////////////////////////////////

class TeleportGuard {
 public:
  explicit TeleportGuard(const IExecutorPtr& e);

  ~TeleportGuard();

 private:
  IExecutorPtr home_;
};

}  // namespace await::fibers
