#pragma once

#include <await/fibers/core/suspend.hpp>
#include <await/fibers/core/check.hpp>

#include <await/support/thread_spinlock.hpp>
#include <await/support/mutex_holder.hpp>

#include <wheels/support/intrusive_list.hpp>
#include <wheels/support/assert.hpp>

#include <deque>
#include <optional>

/*
 * https://www.youtube.com/watch?v=cdpQMDgQP8Y
 * https://github.com/golang/go/issues/8899
 */

namespace await::fibers {

namespace detail {

template <typename T>
class ChannelImpl {
 public:
  struct IReceiver {
    virtual ~IReceiver() = default;

    // Returns false if receiver does not need value anymore
    virtual bool Receive(T& value) = 0;
  };

  struct Receiver : public IReceiver,
                    public wheels::IntrusiveListNode<Receiver> {};

 private:
  using Mutex = support::ThreadSpinLock;
  using UniqueLock = support::MutexHolder<Mutex>;

  // Receiver

  class FiberReceiver : public IAwaiter, public Receiver {
   public:
    FiberReceiver(UniqueLock lock) : lock_(std::move(lock)) {
    }

    // IAwaiter
    void ScheduleResume(FiberHandle f) override {
      // NB: order matters
      fiber_ = f;
      lock_.unlock();
      // Now fiber f is ready to receive value from sender
    }

    // IReceiver
    bool Receive(T& value) override {
      WHEELS_VERIFY(fiber_.IsValid(), "Invalid fiber handle");
      value_.emplace(std::move(value));
      fiber_.Resume();
      return true;
    }

    T ExtractValue() {
      return std::move(*value_);
    }

   private:
    UniqueLock lock_;
    std::optional<T> value_;
    FiberHandle fiber_;
  };

  // Sender

  class Sender : public IAwaiter, public wheels::IntrusiveListNode<Sender> {
   public:
    Sender(T&& value, UniqueLock lock)
        : value_(std::move(value)), lock_(std::move(lock)) {
    }

    void ScheduleResume(FiberHandle f) override {
      fiber_ = f;
      lock_.unlock();
    }

    void Resume() {
      fiber_.Resume();
    }

    FiberHandle Handle() {
      return fiber_;
    }

    T value_;

   private:
    UniqueLock lock_;
    FiberHandle fiber_;
  };

 public:
  ChannelImpl(size_t capacity) : capacity_(capacity) {
    WHEELS_VERIFY(capacity > 0, "Capacity == 0");
  }

  ~ChannelImpl() {
    WHEELS_VERIFY(receivers_.IsEmpty(), "Receivers list is not empty");
  }

  void Send(T value) {
    I_EXPECT_AWAIT_FIBERS;

    UniqueLock lock(mutex_);

    CheckInvariants();

    if (TrySendLocked(value)) {
      return;
    }

    // 3) Suspend sender

    WHEELS_VERIFY(buffer_.size() == capacity_, "Broken channel");

    Sender sender(std::move(value), std::move(lock));
    senders_.PushBack(&sender);
    Suspend(&sender);
  }

  bool TrySend(T value) {
    I_EXPECT_AWAIT_FIBERS;

    UniqueLock lock(mutex_);

    CheckInvariants();
    return TrySendLocked(value);
  }

  // TODO: std::optional<T> + Close
  T Receive() {
    I_EXPECT_AWAIT_FIBERS;

    UniqueLock lock(mutex_);

    CheckInvariants();

    if (!buffer_.empty()) {
      T result = std::move(buffer_.front());
      BufferPopFront();
      return result;
    }

    WHEELS_VERIFY(buffer_.empty(), "Broken channel");

    FiberReceiver receiver(std::move(lock));
    receivers_.PushBack(&receiver);
    Suspend(&receiver);
    // NB: lock released
    return receiver.ExtractValue();
  }

  std::optional<T> TryReceive() {
    I_EXPECT_AWAIT_FIBERS;

    UniqueLock lock(mutex_);

    CheckInvariants();

    if (buffer_.empty()) {
      return std::nullopt;
    }

    T value = std::move(buffer_.front());
    BufferPopFront();
    return value;
  }

  // For Select

  void Receive(Receiver* receiver) {
    UniqueLock lock(mutex_);

    CheckInvariants();

    if (!buffer_.empty()) {
      if (receiver->Receive(buffer_.front())) {
        BufferPopFront();
      }
      return;
    }

    receivers_.PushBack(receiver);
  }

  UniqueLock Lock() {
    return UniqueLock{mutex_};
  }

 private:
  // With mutex_
  bool TrySendLocked(T& value) {
    // 1) Send `value` directly to the receiver

    while (!receivers_.IsEmpty()) {
      Receiver* receiver = receivers_.PopFront();
      // NB: lock should be acquired!
      if (receiver->Receive(value)) {
        return true;  // Received
      }
    }

    // 2) Put `value` to buffer

    if (buffer_.size() < capacity_) {
      buffer_.push_back(std::move(value));
      return true;
    }

    return false;
  }

  // With mutex_
  void BufferPopFront() {
    buffer_.pop_front();

    if (!senders_.IsEmpty()) {
      Sender* sender = senders_.PopFront();
      buffer_.push_back(std::move(sender->value_));
      sender->Resume();
    }
  }

  // With mutex_
  void CheckInvariants() {
    WHEELS_VERIFY(receivers_.IsEmpty() || senders_.IsEmpty(), "Broken channel");
    // !senders.IsEmpty() => buffer_.size() == capacity
    WHEELS_VERIFY(senders_.IsEmpty() ||
                      (!senders_.IsEmpty() && buffer_.size() == capacity_),
                  "Broken channel");
    // !receivers.IsEmpty() => buffer_.empty()
    WHEELS_VERIFY(
        receivers_.IsEmpty() || (!receivers_.IsEmpty() && buffer_.empty()),
        "Broken channel");
  }

 private:
  const size_t capacity_;

  support::ThreadSpinLock mutex_;
  std::deque<T> buffer_;
  wheels::IntrusiveList<Receiver> receivers_;
  wheels::IntrusiveList<Sender> senders_;
};

}  // namespace detail

}  // namespace await::fibers
