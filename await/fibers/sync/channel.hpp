#pragma once

#include <await/fibers/sync/channel_impl.hpp>

#include <memory>

namespace await::fibers {

// Buffered channel (~Golang)

// Does not support void type
// Use wheels::Unit instead (from <wheels/support/unit.hpp>)

template <typename T>
class Channel {
  using Impl = detail::ChannelImpl<T>;

 public:
  // Bounded channel, `capacity` > 0
  Channel(size_t capacity) : impl_(std::make_shared<Impl>(capacity)) {
  }

  // Unbounded channel
  Channel() : Channel(std::numeric_limits<size_t>::max()) {
  }

  // Blocking
  void Send(T value) {
    impl_->Send(std::move(value));
  }

  // Non-blocking
  bool TrySend(T value) {
    return impl_->TrySend(std::move(value));
  }

  // Blocking
  T Receive() {
    return impl_->Receive();
  }

  // Non-blocking
  std::optional<T> TryReceive() {
    return impl_->TryReceive();
  }

  // Workaround for Select
  std::shared_ptr<Impl> AccessImpl() {
    return impl_;
  }

 private:
  std::shared_ptr<Impl> impl_;
};

}  // namespace await::fibers
