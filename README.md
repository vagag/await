# Await

Concurrency for C++:
- Executors
- Fibers
- Futures + combinators (`All`, `FirstOf`, `Quorum`)
- Customizable `Await`
- Channels + `Select`

## TODO

- Structured concurrency

## Inspiration 

- [Asymmetric Transfer](https://lewissbaker.github.io/)
- [Project Loom Proposal](http://cr.openjdk.java.net/~rpressler/loom/Loom-Proposal.html), State of Loom: [Part 1](https://cr.openjdk.java.net/~rpressler/loom/loom/sol1_part1.html), [Part 2](https://cr.openjdk.java.net/~rpressler/loom/loom/sol1_part2.html)
- [Your Server as a Function](https://monkey.org/~marius/funsrv.pdf)
- Folly: [Futures](https://github.com/facebook/folly/tree/master/folly/futures), [Executors](https://github.com/facebook/folly/tree/master/folly/executors), [Fibers](https://github.com/facebook/folly/tree/master/folly/fibers)
- [Асинхронность в программировании](https://habr.com/ru/company/jugru/blog/446562/)

## Requirements

- x86-64
- Clang++ (>= 8)

## Dependencies

- [Wheels](https://gitlab.com/Lipovsky/wheels) – core components, test framework
- [Twist](https://gitlab.com/Lipovsky/twist) – fault injection

## Build

```shell
# Clone repo
git clone https://gitlab.com/Lipovsky/await.git 
cd await
# Generate build files
mkdir build && cd build
cmake -DAWAIT_TESTS=ON ..
# Build tests
make await_tests
# Run tests
./tests/wheels/bin/await_tests
