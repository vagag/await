#include <await/fibers/core/api.hpp>
#include <await/fibers/core/await.hpp>
#include <await/fibers/core/fls.hpp>
#include <await/fibers/core/guts.hpp>

#include <await/fibers/sync/future.hpp>
#include <await/fibers/sync/spinlock.hpp>
#include <await/fibers/sync/mutex.hpp>
#include <await/fibers/sync/condvar.hpp>
#include <await/fibers/sync/thread_like.hpp>
#include <await/fibers/sync/teleport.hpp>

#include <await/fibers/test/test_runtime.hpp>

#include <await/time/time_keeper.hpp>

#include <await/executors/label_thread.hpp>
#include <await/executors/static_thread_pool.hpp>
#include <await/executors/strand.hpp>
#include <await/executors/inline.hpp>
#include <await/executors/manual.hpp>

#include <wheels/test/test_framework.hpp>
#include <await/fibers/test/test.hpp>

#include <chrono>

using namespace await::executors;
using namespace await::fibers;
using namespace std::chrono_literals;
using wheels::test::TestOptions;

static void SleepFor(wheels::Duration d) {
  static await::time::TimeKeeper time_keeper;
  Await(time_keeper.After(d)).ExpectOk();
}

TEST_SUITE(Fibers) {
  SIMPLE_TEST(HelloWorld) {
    auto tp = MakeStaticThreadPool(1, "test");
    Spawn(
        []() {
          std::cout << "Hello!" << std::endl;
        },
        tp);
    tp->Join();
  }

  SIMPLE_TEST(Spawns) {
    bool done{false};
    auto tp = MakeStaticThreadPool(2, "test");

    Spawn(
        [&]() {
          // Spawn in current executor
          Spawn([&]() {
            done = true;
          });
        },
        tp);

    tp->Join();

    ASSERT_TRUE(done);
  }

  SIMPLE_TEST(TestRuntime) {
    TestRuntime runtime{1};
    runtime.Spawn([]() {
      std::cout << "Hello!" << std::endl;
    });
    runtime.Join();
  }

  template <typename Mutex>
  void TestMutexType() {
    static const size_t kThreads = 4;
    static const size_t kFibers = 512;
    static const size_t kIncrements = 1024;

    TestRuntime runtime{kThreads};

    Mutex mutex;
    std::atomic<size_t> shared_counter = 0;

    for (size_t i = 0; i < kFibers; ++i) {
      runtime.Spawn([&shared_counter, &mutex]() {
        for (size_t k = 0; k < kIncrements; ++k) {
          {
            auto guard = mutex.Guard();
            shared_counter.store(shared_counter.load() + 1);
          }
          if (k % 17 == 0) {
            self::Yield();
          }
        }
      });
    }

    runtime.Join();

    std::cout << "Shared counter value: " << shared_counter.load() << std::endl;
    std::cout << "Increments made: " << kFibers * kIncrements << std::endl;

    ASSERT_EQ(shared_counter.load(), kFibers * kIncrements);
  }

  TEST(SpinLock, TestOptions().TimeLimit(30s)) {
    TestMutexType<SpinLock>();
  }

  TEST(Mutex, TestOptions().TimeLimit(30s)) {
    TestMutexType<Mutex>();
  }

  SIMPLE_FIBER_TEST(MutexTryLock, 1) {
    Mutex mutex;

    ASSERT_TRUE(mutex.TryLock());
    mutex.Unlock();

    mutex.Lock();
    ASSERT_FALSE(mutex.TryLock());
    mutex.Unlock();
  }

#ifndef NDEBUG
  SIMPLE_FIBER_TEST(MutexOwnedByThisFiber, 1) {
    Mutex mutex;

    ASSERT_FALSE(mutex.OwnedByThisFiber());
    mutex.Lock();
    ASSERT_TRUE(mutex.OwnedByThisFiber());

    ThreadLike t([&mutex]() {
      ASSERT_FALSE(mutex.TryLock());
      ASSERT_FALSE(mutex.OwnedByThisFiber());
    });
    t.Join();

    mutex.unlock();
    ASSERT_FALSE(mutex.OwnedByThisFiber());
  }
#endif

  SIMPLE_FIBER_TEST(Sleep, 1) {
    SleepFor(1s);
    std::cout << "Hello after sleep!" << std::endl;
  }

  SIMPLE_FIBER_TEST(ThreadLike, 1) {
    TestRuntime runtime{1};

    auto main = [&runtime]() {
      auto time_passed = false;

      auto child = runtime.Thread([&time_passed]() {
        SleepFor(1s);
        time_passed = true;
      });

      child.Join();
      ASSERT_TRUE(time_passed);
    };

    runtime.Spawn(main);
    runtime.Join();
  }

  SIMPLE_TEST(FiberHandleInvalid) {
    auto h = FiberHandle::Invalid();
    ASSERT_TRUE(!h.IsValid());
  }

  SIMPLE_FIBER_TEST(Id, 1) {
    FiberId id = self::GetId();
    ASSERT_EQ(self::GetId(), id);  // Still the same

    Spawn([parent_id = id]() {
      ASSERT_NE(self::GetId(), parent_id);  // Different ids
    });
  }

  SIMPLE_FIBER_TEST(Names, 1) {
    ASSERT_FALSE(self::GetName().has_value());
    self::SetName("tester");
    ASSERT_EQ(*self::GetName(), "tester");
  }

  SIMPLE_FIBER_TEST(FLS, 1) {
    ASSERT_FALSE(self::GetLocal<std::string>("k").has_value());
    self::SetLocal("k", std::string("v"));
    auto local = self::GetLocal<std::string>("k");
    ASSERT_TRUE(local.has_value());
    ASSERT_EQ(*local, "v");
  }

  SIMPLE_TEST(TeleportGuard) {
    IThreadPoolPtr tp = MakeStaticThreadPool(3, "outer");

    TestRuntime runtime{1, "test"};

    runtime.Spawn([&tp]() {
      ASSERT_EQ(GetThreadLabel(), "test");
      {
        TeleportGuard t(tp);
        ASSERT_EQ(GetThreadLabel(), "outer");
        std::cout << "Teleported to thread pool: " << GetThreadLabel()
                  << std::endl;
      }
      std::cout << "Teleported back to thread pool: " << GetThreadLabel()
                << std::endl;
      ASSERT_EQ(GetThreadLabel(), "test");
    });

    runtime.Join();
  }

#if !(__has_feature(thread_sanitizer))

  SIMPLE_TEST(StackPooling) {
    static const size_t kFibers = 123456;

    size_t completed{0};

    auto e = GetInlineExecutor();

    for (size_t i = 0; i < kFibers; ++i) {
      Spawn(
          [&completed]() {
            completed++;
          },
          e);
    }

    ASSERT_EQ(completed, kFibers);
  }

#endif

  SIMPLE_TEST(Manual) {
    auto manual = MakeManualExecutor();

    auto routine = []() {
      self::Yield();
      self::Yield();
      self::Yield();
    };

    Spawn(routine, manual);
    Spawn(routine, manual);

    size_t steps = 0;
    while (manual->MakeStep()) {
      ++steps;
    }

    ASSERT_EQ(steps, 8);
  }

  SIMPLE_TEST(AliveFibers) {
    auto manual = MakeManualExecutor();

    auto routine1 = []() {
      // 1
      self::Yield();
      // 3
    };

    auto routine2 = []() {
      // 2
      self::Yield();
      // 4
      self::Yield();
      // 5
    };

    Spawn(routine1, manual);
    Spawn(routine2, manual);

    ASSERT_EQ(AliveFibers().Size(), 2);

    manual->MakeSteps(3);

    ASSERT_EQ(AliveFibers().Size(), 1);

    manual->MakeSteps(999);

    ASSERT_TRUE(AliveFibers().IsEmpty());
  }
}

TEST_SUITE(CondVar) {
  SIMPLE_TEST(RoundRobin) {
    static const size_t kThreads = 3;

    TestRuntime runtime{kThreads};

    static const size_t kSteps = 1024;
    static const size_t kFibers = 5;

    Mutex mutex;
    CondVar cond;
    size_t turn = 0;
    size_t steps = 0;

    auto routine = [&](size_t i) {
      for (size_t k = 0; k < kSteps; ++k) {
        std::unique_lock lock(mutex);

        while (turn != i) {
          cond.Wait(lock);
        }

        ASSERT_TRUE(turn == i);
        ++steps;
        turn = (turn + 1) % kFibers;
        cond.NotifyAll();
      }
    };

    for (size_t i = 0; i < kFibers; ++i) {
      runtime.Spawn([routine, i]() {
        routine(i);
      });
    }

    runtime.Join();

    ASSERT_EQ(steps, kSteps * kFibers);
  }
}
