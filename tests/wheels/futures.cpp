#include <wheels/test/test_framework.hpp>

#include <await/futures/promise.hpp>
#include <await/futures/await.hpp>

#include <await/futures/helpers.hpp>

#include <await/futures/combine/all.hpp>
#include <await/futures/combine/quorum.hpp>

#include <await/executors/static_thread_pool.hpp>
#include <await/executors/label_thread.hpp>

#include <await/time/time_keeper.hpp>

using namespace std::chrono_literals;
using namespace await::futures;
using namespace await::executors;
using wheels::Status;

// Helpers

wheels::Error MakeTestError() {
  return {std::make_error_code(std::errc::timed_out)};
}

struct MoveOnly {
  MoveOnly(const MoveOnly& that) = delete;
  MoveOnly& operator=(const MoveOnly& that) = delete;

  MoveOnly(MoveOnly&& that) = default;

  std::string data;
};

TEST_SUITE(Futures) {
  // Basic

  SIMPLE_TEST(JustWorks) {
    auto [f, p] = MakeContract<int>();
    ASSERT_FALSE(f.IsReady());
    std::move(p).SetValue(42);
    ASSERT_EQ(std::move(f).GetReadyResult(), 42);
  }

  SIMPLE_TEST(GetAsyncResult) {
    await::time::TimeKeeper tk;

    auto f = tk.After(1s).Then([](Status /*status*/) -> int {
      return 42;
    });

    auto result = std::move(f).GetResult();
    ASSERT_EQ(*result, 42);
  }

  SIMPLE_TEST(Then) {
    auto [f, p] = MakeContract<int>();
    ASSERT_FALSE(f.IsReady());
    auto g = std::move(f).Then([](Result<int> result) {
      return result.Value() + 1;
    });
    std::move(p).SetValue(9);
    ASSERT_EQ(std::move(g).GetResult(), 10);
  }

  // Helpers

  SIMPLE_TEST(Void) {
    auto [f, p] = MakeContract<void>();
    std::move(p).Set();
    std::move(f).GetReadyResult().ExpectOk();
  }

  SIMPLE_TEST(AsyncVia) {
    auto tp = MakeStaticThreadPool(4, "test");

    {
      // Return value

      auto f = AsyncVia(tp, []() {
        ExpectThread("test");
        return 42;
      });
      ASSERT_EQ(std::move(f).GetResult().Value(), 42);
    }

    {
      // Void

      bool done = false;
      auto f = AsyncVia(tp, [&done]() {
        ExpectThread("test");
        done = true;
        return;
      });

      Await(std::move(f)).ExpectOk();

      ASSERT_EQ(done, true);
    }

    tp->Join();
  }

  SIMPLE_TEST(JustStatus) {
    {
      // Ok
      auto [f, p] = MakeContract<int>();
      auto jsf = JustStatus(std::move(f));
      std::move(p).SetValue(42);

      Status status = std::move(jsf).GetResult();

      ASSERT_TRUE(status.IsOk());
    }

    {
      // Error
      auto [f, p] = MakeContract<int>();
      auto jsf = JustStatus(std::move(f));
      std::move(p).SetError(MakeTestError());

      Status status = std::move(jsf).GetResult();
      ASSERT_TRUE(status.HasError());
    }
  }

  SIMPLE_TEST(MakeValue) {
    auto f = MakeValue(42);
    ASSERT_TRUE(f.IsReady());
    auto result = std::move(f).GetReadyResult();
    ASSERT_EQ(*result, 42);
  }

  SIMPLE_TEST(MakeCompletedVoid) {
    Future<void> f = MakeCompletedVoid();
    ASSERT_TRUE(f.IsReady());
    Status status = std::move(f).GetReadyResult();
    ASSERT_TRUE(status.IsOk());
  }

  SIMPLE_TEST(MakeTestError) {
    // Automatically convert Failure to Future<int>
    Future<int> f = MakeError(std::make_error_code(std::errc::timed_out));

    ASSERT_TRUE(f.IsReady());
    auto result = std::move(f).GetReadyResult();
    ASSERT_TRUE(result.HasError());
  }

  SIMPLE_TEST(MakeErrorFailure) {
    auto failure = MakeError(std::make_error_code(std::errc::timed_out));
    // Does not compile
    // failure.As<int>();

    auto future = std::move(failure).As<int>();
    ASSERT_TRUE(future.IsReady());
  }

  // Combinators

  SIMPLE_TEST(All) {
    std::vector<Promise<int>> promises{3};

    std::vector<Future<int>> futures;
    for (auto& p : promises) {
      futures.push_back(p.MakeFuture());
    }

    auto all = All(std::move(futures));

    ASSERT_FALSE(all.IsReady());

    std::move(promises[2]).SetValue(7);
    std::move(promises[0]).SetValue(3);

    // Still not completed
    ASSERT_FALSE(all.IsReady());

    std::move(promises[1]).SetValue(5);

    ASSERT_TRUE(all.IsReady());

    Result<std::vector<int>> ints_result = std::move(all).GetResult();
    ASSERT_TRUE(ints_result.IsOk());

    std::vector<int> ints = *ints_result;
    ASSERT_EQ(ints, std::vector<int>({7, 3, 5}));
  }

  SIMPLE_TEST(AllFails) {
    std::vector<Promise<int>> promises{3};

    std::vector<Future<int>> futures;
    for (auto& p : promises) {
      futures.push_back(p.MakeFuture());
    }

    auto all = All(std::move(futures));

    ASSERT_FALSE(all.IsReady());

    // First error
    std::move(promises[1]).SetError(MakeTestError());
    ASSERT_TRUE(all.IsReady());

    // Second error
    std::move(promises[0]).SetError(MakeTestError());

    auto all_result = std::move(all).GetResult();
    ASSERT_TRUE(all_result.HasError());
  }

  SIMPLE_TEST(AllVoid) {
    std::vector<Promise<void>> promises{2};

    std::vector<Future<void>> futures;
    for (auto& p : promises) {
      futures.push_back(p.MakeFuture());
    }

    Future<void> all = All(std::move(futures));

    std::move(promises[0]).Set();
    ASSERT_FALSE(all.IsReady());

    std::move(promises[1]).Set();
    ASSERT_TRUE(all.IsReady());

    ASSERT_TRUE(std::move(all).GetReadyResult().IsOk());
  }

  SIMPLE_TEST(AllEmptyInput) {
    auto all = All(std::vector<Future<int>>{});

    ASSERT_TRUE(all.IsReady());
    ASSERT_TRUE(std::move(all).GetResult().IsOk());
  }

  SIMPLE_TEST(Quorum) {
    std::vector<Promise<int>> promises{5};

    std::vector<Future<int>> futures;
    for (auto& p : promises) {
      futures.push_back(p.MakeFuture());
    }

    Future<std::vector<int>> q = Quorum(std::move(futures), 3);

    // 1/3
    std::move(promises[2]).SetValue(17);
    // 2/3
    std::move(promises[4]).SetValue(42);

    ASSERT_FALSE(q.IsReady());

    // 3/3
    std::move(promises[0]).SetValue(256);

    ASSERT_TRUE(q.IsReady());

    // > threshold
    std::move(promises[1]).SetValue(13);

    auto q_result = std::move(q).GetReadyResult();
    ASSERT_TRUE(q_result.IsOk());

    auto q_values = *q_result;

    ASSERT_EQ(q_values.size(), 3);
    ASSERT_EQ(q_values[0], 17);
    ASSERT_EQ(q_values[1], 42);
    ASSERT_EQ(q_values[2], 256);
  }

  SIMPLE_TEST(QuorumVoid) {
    std::vector<Promise<void>> promises{5};

    std::vector<Future<void>> futures;
    for (auto& p : promises) {
      futures.push_back(p.MakeFuture());
    }

    Future<void> q = Quorum(std::move(futures), 3);

    std::move(promises[2]).Set();
    std::move(promises[4]).Set();

    ASSERT_FALSE(q.IsReady());

    std::move(promises[0]).Set();

    ASSERT_TRUE(q.IsReady());
    ASSERT_TRUE(std::move(q).GetResult().IsOk());
  }

  SIMPLE_TEST(RecoverWith) {
    Future<int> f = ::await::futures::MakeError(MakeTestError());
    auto g = RecoverWith(std::move(f), [](const wheels::Error& /*error*/) {
      return MakeValue<int>(42);
    });

    ASSERT_EQ(std::move(g).GetResult(), 42);
  }

  SIMPLE_TEST(Via) {
    auto tp = MakeStaticThreadPool(2, "test");

    auto [f, p] = MakeContract<void>();

    auto answer = std::move(f).Via(tp).Then([](Status /*status*/) {
      ExpectThread("test");
      return 42;
    });

    std::move(p).Set();
    ASSERT_EQ(std::move(answer).GetResult().Value(), 42);

    tp->Join();
  }

  SIMPLE_TEST(ThenExecutor) {
    auto tp = MakeStaticThreadPool(1, "test");
    auto [f, p] = MakeContract<void>();
    auto g = std::move(f).Via(tp).Then([](Status) {
      return 42;
    });
    ASSERT_EQ(g.GetExecutor(), tp);
  }

  SIMPLE_TEST(SubscribeConst) {
    auto [f, p] = MakeContract<MoveOnly>();

    auto print = [](const Result<MoveOnly>& result) {
      std::cout << "Result: " << result.Value().data << std::endl;
    };

    auto _f = SubscribeConst(std::move(f), std::move(print));
    std::move(p).SetValue({"Hello"});

    ASSERT_EQ(std::move(_f).GetResult().Value().data, "Hello");
  }

  SIMPLE_TEST(AfterAfter) {
    await::time::TimeKeeper tk;

    auto f = tk.After(1s)
                 .Then([&](Status) {
                   return tk.After(1s);
                 })
                 .Then([](Status) {
                   return 42;
                 });

    ASSERT_EQ(Await(std::move(f)).Value(), 42);
  }

  SIMPLE_TEST(WithInterrupt) {
    await::time::TimeKeeper tk;

    {
      // Value
      auto timeout = tk.After(2s);
      auto value = tk.After(1s).Then([](Status) {
        return 42;
      });

      auto with_timeout = WithInterrupt(std::move(value), std::move(timeout));

      auto result = Await(std::move(with_timeout));

      ASSERT_TRUE(result.IsOk());
      ASSERT_EQ(result.Value(), 42);
    }

    {
      // Timeout
      auto timeout = tk.After(1s);
      auto value = tk.After(2s).Then([](Status) {
        return 42;
      });

      auto with_timeout = WithInterrupt(std::move(value), std::move(timeout));

      auto result = Await(std::move(with_timeout));

      ASSERT_FALSE(result.IsOk());
      ASSERT_TRUE(result.MatchErrorCode((int)std::errc::interrupted));
    }
  }
}
